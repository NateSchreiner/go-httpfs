package auth

type AuthInfo struct {
	keyholder  string
	privileges []string
}
