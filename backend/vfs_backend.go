package backend

import (
	"github.com/spf13/afero"
	"gitlab.com/NateSchreiner/go-httpfs/auth"
	"gitlab.com/NateSchreiner/go-httpfs/config"
)

// TODO :: use "github.com/spf13/afero"
type VfsBackend struct {
	Config   config.Config
	AuthInfo map[string]auth.AuthInfo
	Fs       afero.Fs
}

func Init() *VfsBackend {
	app := afero.NewMemMapFs()

	vfs := &VfsBackend{
		Config:   config.Config{},
		AuthInfo: nil,
		Fs:       app,
	}

	return vfs
}

func (vfs *VfsBackend) ListDir(path string, depth int) ([]HfsItem, error) {
	return nil, nil
}

func (vfs *VfsBackend) GetFile(path string) (*HfsItem, error) {
	return nil, nil
}

func (vfs *VfsBackend) AppendFile(path string, content []byte) error {
	return nil
}

func (vfs *VfsBackend) CreateFile(item HfsItem, path string) error {
	return nil
}

func (vfs *VfsBackend) CreateDir(path string) error {
	return nil
}

func (vfs *VfsBackend) DeleteDir() error {
	return nil
}

func (vfs *VfsBackend) DeleteFile() error {
	return nil
}

func (vfs *VfsBackend) MoveFile() error {
	return nil
}

func (vfs *VfsBackend) CopyFile() error {
	return nil
}
