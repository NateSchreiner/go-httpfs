package backend

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/goccy/go-json"
	"golang.org/x/exp/slog"
	"kr.dev/walk"

	"gitlab.com/NateSchreiner/go-httpfs/auth"
	"gitlab.com/NateSchreiner/go-httpfs/config"
)

// Each file is going to need a mutex?
// Will the dirs themselves? -> Probably
type FsBackend struct {
	Config   config.Config
	AuthInfo map[string]auth.AuthInfo
}

// TODO:: Strip trailing slashes from input
// What to do about depth? How do we show depth? Do we return a tree to the user?
func (b *FsBackend) ListDir(path string, depth int) ([]HfsItem, error) {
	list := make([]HfsItem, 0)

	walker := walk.New(os.DirFS(b.Config.Root), path)
	index := 0

	for walker.Next() {
		if index == 0 {
			index++
			continue
		}

		if err := walker.Err(); err != nil {
			log.Fatalf(err.Error())
			return nil, err
		}
		entry := walker.Entry()
		fmt.Printf("Looking at Entry: %s\n", entry.Name())
		if path == entry.Name() {
			continue
		}

		fp := filepath.Join(b.Config.Root, walker.Path())
		stat, err := os.Stat(fp)
		if err != nil {
			log.Fatalf(err.Error())
			return nil, err
		}

		if entry.IsDir() {
			if depth <= 1 {
				walker.SkipDir()
			} else {
				b.ListDir(fp, depth-1)
			}
		}

		i := &HfsItem{
			Name:    stat.Name(),
			Size:    stat.Size(),
			Mode:    uint32(stat.Mode()),
			IsDir:   stat.IsDir(),
			ModTime: stat.ModTime(),
			Content: "",
		}

		list = append(list, *i)
	}

	return list, nil
}

func (b *FsBackend) AppendFile(path string, content []byte) error {
	full := filepath.Join(b.Config.Root, path)

	slog.Info("Appending to: ", "path", full)
	f, err := os.OpenFile(full, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	defer f.Close()

	n, err := f.Write(content)
	if err != nil {
		slog.Error(err.Error())
		return err
	}

	if n != len(content) {
		return fmt.Errorf("expected to write %d bytes actually wrote %d", len(content), n)
	}

	return nil
}

func (b *FsBackend) CreateFile(item HfsItem, path string) error {
	_, err := os.Stat(path)
	if err != nil {
		err = os.MkdirAll(path, 0777)
		if err != nil {
			return err
		}
	}

	f, err := os.Create(filepath.Join(path, item.Name))
	if err != nil {
		return err
	}

	n, err := f.Write([]byte(item.Content))
	if err != nil {
		return err
	}

	if n != len(item.Content) {
		return fmt.Errorf("expected to write %d bytes actually wrote %d bytes", len(item.Content), n)
	}

	return nil
}

func (b *FsBackend) CreateDir(path string) error {
	err := os.MkdirAll(path, 0777)
	if err != nil {
		return err
	}

	return nil
}

func (b *FsBackend) ReadFile(path string) ([]byte, error) {
	fp := filepath.Join(b.Config.Root, path)
	// check that filepath exists
	fpStat, err := os.Stat(fp)
	if err != nil {
		return nil, err
	}

	if fpStat.IsDir() {
		return nil, errors.New("trying to read directory is not supported. For directories, use `list` command")
	}

	r := make([]byte, fpStat.Size())

	f, err := os.Open(fp)
	if err != nil {
		return nil, err
	}

	n, err := f.Read(r)
	if err != nil {
		return nil, err
	}

	if int64(n) != fpStat.Size() {
		// probably log here?
	}

	//rI := make(map[HfsItem][]byte, 1)

	i := &HfsItem{
		Name:    fpStat.Name(),
		Size:    fpStat.Size(),
		Mode:    uint32(fpStat.Mode()),
		IsDir:   false,
		ModTime: fpStat.ModTime(),
		Content: string(r),
	}

	json, err := json.Marshal(i)

	return json, nil
}

func (b *FsBackend) GetFile(path string) (*HfsItem, error) {
	fp := filepath.Join(b.Config.Root, path)
	// check that filepath exists
	fpStat, err := os.Stat(fp)
	if err != nil {
		return nil, err
	}

	if fpStat.IsDir() {
		return nil, errors.New("trying to read directory is not supported. For directories, use `list` command")
	}

	r := make([]byte, fpStat.Size())

	f, err := os.Open(fp)
	if err != nil {
		return nil, err
	}

	n, err := f.Read(r)
	if err != nil {
		return nil, err
	}

	if int64(n) != fpStat.Size() {
		slog.Warn("Content size not matching", "expected", fpStat.Size(), "got", n)
	}

	i := &HfsItem{
		Name:    fpStat.Name(),
		Size:    fpStat.Size(),
		Mode:    uint32(fpStat.Mode()),
		IsDir:   false,
		ModTime: fpStat.ModTime(),
		Content: string(r),
	}

	//json, err := json.Marshal(i)

	return i, nil
}

func (b *FsBackend) DeleteFile() error {
	return errors.New("TODO")
}

func (b *FsBackend) DeleteDir() error {
	return errors.New("TODO")
}

func (b *FsBackend) MoveFile() error {
	return errors.New("TODO")
}

func (b *FsBackend) CopyFile() error {
	return errors.New("TODO")
}
