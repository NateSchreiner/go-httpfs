package backend

import "time"

// Name() string       // base name of the file
// Size() int64        // length in bytes for regular files; system-dependent for others
// Mode() FileMode     // file mode bits
// ModTime() time.Time // modification time
// IsDir() bool        // abbreviation for Mode().IsDir()
// Sys() any           // underlying data source (can return nil)
type HfsItem struct {
	Name    string
	Size    int64
	Mode    uint32
	IsDir   bool
	ModTime time.Time
	Content string
}
