package backend

type Backend interface {
	ListDir(path string, depth int) ([]HfsItem, error)
	GetFile(path string) (*HfsItem, error)
	AppendFile(path string, content []byte) error
	CreateFile(item HfsItem, path string) error
	CreateDir(path string) error
	DeleteDir() error
	DeleteFile() error
	MoveFile() error
	CopyFile() error
}
