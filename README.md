# HTTPFS Server
>Interact with a remote HTTP server just as you would a local filesystem

- Runs entirely in user-space
- Provides isolation level for concurrent file access stronger than the one mandated by POSIX file system semantics. Close to that of AFS

# Alternatives / Prior work

### WebFS : https://github.com/brendoncarroll/webfs
### SpockFS : https://github.com/unbit/spockfs
### webDAV

### https://www.usenix.org/legacy/publications/library/proceedings/usenix99/full_papers/kiselyov/kiselyov.pdf

### https://opensource.com/article/19/3/virtual-filesystems-linux

### https://github.com/bigfile/bigfile


**That means you can perform the following operations:**
- read
- create
- write
- append
- delete
- move
- copy


* A user would create a "mount" or a "root" (directory or file)
>On the server this would be a new VFS

* I don't know if a VFS makes sense here? I guess I don't really even understand what a VFS does fully? For now we will just pick an application root.  All users directories will be created as a child of this root dir and each users directory can optionally be locked down. 

* A user can then add files/directories to this VFS, delete the mount, etc..
>Since this is HTTP, uploading a whole directory probably doesn't make sense unless it's 
> zipped up? Client would have to add header: `Content-type: application/zip` in which case
> server will unzip and handle the directory accordingly

* Copy an existing mount to a new mount? Permissions..?

* What about very large files? Network can be flakey?
>Maybe implement partial requests
>https://datatracker.ietf.org/doc/html/rfc7233#page-4

## Things to Explore
- Can our security model benefit from using `chroot` ?
- Is there a better way to handle creation / movement editing other than directly 
interacting with the native FS? 
  - Right now: Client -> (UserRequest) -> Server -> <Process> -> Native Filesystem
  >Does a VFS help us in any way here? What are the (if any) downsides to the above
   model? Does speed matter here? Can we even speed it up?


## Implementation Details

### HTTP Methods
GET / PUT / HEAD / DELETE

### Architecture

CMD -> Server  --> Backend 
Requests -^


## Security Concerns
1. Seems dangerous that by default the world can view / download anything you upload 
to the server? 
2. Probably use Public/private key and assign a private key to each directory created? 
3. Would it be beneficial to lockdown directories based on IP ranges? 

## TODO
1. Graceful shutdowns? Make certain we not processing a request? 