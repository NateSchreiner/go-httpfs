package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/NateSchreiner/go-httpfs/config"
	"gitlab.com/NateSchreiner/go-httpfs/httpfs"
)

func main() {
	args := os.Args
	var conf config.Config
	if len(args) > 1 {
		// mUst be a config path?
		conf = config.LoadConfig(args[1])
	} else {
		conf = config.LoadConfig("default.json")
	}

	server := httpfs.NewServer(conf)

	fmt.Printf("HTTP-FS listening on Port: %s \n", conf.Port)

	err := http.ListenAndServe(fmt.Sprintf(":%s", conf.Port), server)
	if err != nil {
		panic(err)
	}

}
