module gitlab.com/NateSchreiner/go-httpfs

go 1.19

require (
	github.com/goccy/go-json v0.10.2
	golang.org/x/exp v0.0.0-20230817173708-d852ddb80c63
	kr.dev/walk v0.1.0
)

require (
	github.com/spf13/afero v1.10.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)
