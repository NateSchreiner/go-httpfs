package config

import (
	"log"
	"os"

	"github.com/goccy/go-json"
)

// Does the server know about requests being blocked by authentication?
// I guess so
type Config struct {
	Root      string
	ConfigDir string
	KeyDir    string
	AuthFile  string
	Port      string
	Backend   string
}

func LoadConfig(path string) Config {
	f, err := os.ReadFile(path)
	if err != nil {
		log.Fatalf("Could not read config file: %s", path)
	}

	config := &Config{}
	err = json.Unmarshal(f, config)
	if err != nil {
		log.Fatalf("Could not unmarshal config file: %s", err.Error())
	}

	return *config
}
