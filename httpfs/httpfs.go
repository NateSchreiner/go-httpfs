package httpfs

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/goccy/go-json"
	"gitlab.com/NateSchreiner/go-httpfs/auth"
	"gitlab.com/NateSchreiner/go-httpfs/backend"
	"gitlab.com/NateSchreiner/go-httpfs/config"
	"gitlab.com/NateSchreiner/go-httpfs/logger"
	"golang.org/x/exp/slog"
)

type Action string

const (
	List   Action = "list"   // List cOntents of directory
	Get    Action = "read"   // Read contents of file
	Create Action = "create" // Create dir (Recursive possibly?)
	Upload Action = "upload" // upload single file or ZIP
	Delete Action = "delete" // Delete single file (or dir?)
	Move   Action = "move"   // Move file or dir from one location to another
	Copy   Action = "copy"   // Same as move but makes copy
	Append Action = "append" // Appends to specified file
)

// When a server starts it first reads it's internal storage
// I guess we don't need any internal storage (JSON, DB, etc)
// We will have a default config, which will have the Root
// What about key management? I guess just a key file?

// We actually do need meta data stored somewhere.  Use case is
// a user creates a directory and wants it locked down with a
// password/key.  Server needs a way of knowing this file is blocked
// by authentication

type Server struct {
	http.Handler

	logger  *logger.Logger
	config  config.Config
	backend backend.Backend
}

func NewServer(config config.Config) *Server {
	s := &Server{
		config: config,
	}
	s.loadBackend()

	return s
}

func (s *Server) loadBackend() {
	// Stat the config.root, config.keydir, and config.authFile
	_, err := os.Stat(filepath.Join(s.config.Root))
	if err != nil {
		err := os.Mkdir(filepath.Join(s.config.Root), 0777)
		if err != nil {
			panic(fmt.Sprintf("can't create root filesystem: %s", err.Error()))
		}
	}

	_, err = os.Stat(filepath.Join(s.config.ConfigDir))
	if err != nil {
		err := os.Mkdir(filepath.Join(s.config.ConfigDir), 0777)
		if err != nil {
			panic(fmt.Sprintf("can't create config directory: %s", err.Error()))
		}
	}

	_, err = os.Stat(filepath.Join(s.config.ConfigDir, s.config.KeyDir))
	if err != nil {
		err := os.Mkdir(filepath.Join(s.config.ConfigDir, s.config.KeyDir), 0777)
		if err != nil {
			panic(fmt.Sprintf("can't create key directory: %s", err.Error()))
		}
	}

	authFileStat, err := os.Stat(filepath.Join(s.config.ConfigDir, s.config.AuthFile))
	if err != nil {
		_, err := os.Create(filepath.Join(s.config.ConfigDir, s.config.AuthFile))
		if err != nil {
			panic(fmt.Sprintf("can't create auth file: %s", err.Error()))
		}

		authFileStat, err = os.Stat(filepath.Join(s.config.ConfigDir, s.config.AuthFile))
		if err != nil {
			panic(err)
		}
	}

	authInfo := make(map[string]auth.AuthInfo)

	if authFileStat.Size() > 0 {
		f, err := os.ReadFile(filepath.Join(s.config.ConfigDir, s.config.AuthFile))
		if err != nil {
			panic("Can't open auth file")
		}
		err = json.Unmarshal(f, authInfo)
		if err != nil {
			panic(err)
		}
	}

	b := &backend.FsBackend{
		Config:   s.config,
		AuthInfo: authInfo,
	}

	s.backend = backend.Backend(b)
}

func (s *Server) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	slog.Info("Received request", "method", req.Method)
	query, err := url.ParseQuery(req.URL.RawQuery)
	if err != nil {
		// Can we complete the request despite errors?
		slog.Info("Error ParsingQuery", "error", err)
		writeError(writer, err)
		return
	}
	switch req.Method {
	case "GET":
		s.handleGet(writer, query)
	case "POST":
		s.handlePost(writer, query, req.Body)
	case "PUT":
		body, err := req.MultipartReader()
		if err != nil {
			slog.Warn("MultipartReader", "msg", err)
		}
		s.handlePut(writer, query, body)
	}
}

func (s *Server) handlePut(writer http.ResponseWriter, query url.Values, body *multipart.Reader) {
	// ParseQuery req.URL.RawQuery
	// query, err := url.ParseQuery(req.URL.RawQuery)
	// query := req.URL.Query()
	action := query.Get("action")
	switch action {
	case string(Upload):
		path := query.Get("path")
		err := s.handleFileUpload(body, path)
		if err != nil {
			writeError(writer, err)
			return
		}

	case string(Create):
		// Create dir
		err := s.backend.CreateDir(filepath.Join(s.config.Root, query.Get("path")))
		if err != nil {
			writeError(writer, err)
			return
		}

	default:
		writer.WriteHeader(404)
		writer.Write([]byte(fmt.Sprintf("Could not route command: %s", action)))
		return
	}

	writer.WriteHeader(200)
}

// What are the possible operations here?
// 1. Upload a single file to a specified directory
// 2. Create a new directory (empty) at a specified location
// 3. Append to a file at a specified location
// 4. Move an entire directory or single file from one location to another
// 5. Copy " ....
func (s *Server) handlePost(writer http.ResponseWriter, query url.Values, body io.Reader) {
	if !query.Has("action") {
		writer.WriteHeader(400)
		writer.Write([]byte("Bad request. Needs to include `action` key."))
		return
	}

	action := query.Get("action")
	fmt.Printf("Got Action: %s\n", action)
	switch action {
	case string(Append):
		path := query.Get("path")
		bytes, err := io.ReadAll(body)
		if err != nil {
			panic(err)
		}
		s.backend.AppendFile(path, bytes)
	case string(Move):
		slog.Info("Implement")
	case string(Copy):
		slog.Info("Implement")
	default:
		writer.WriteHeader(404)
		writer.Write([]byte(fmt.Sprintf("Could not route command: %s", action)))
		return

	}

	writer.WriteHeader(200)
}

func (s *Server) handleGet(writer http.ResponseWriter, query url.Values) {
	// Does the request require auth?

	// 1. Look up path in authInfo file.
	// 2. Does any directory in path require auth?
	// 3. Then whole request requires auth
	// query := req.URL.Query()
	header := writer.Header()
	action := query.Get("action")
	header.Set("Accept-Ranges", "bytes") // Used to tell client partial requests are supported. Can range by 'bytes'

	switch action {
	case string(Get):
		returnItem, err := s.backend.GetFile(query.Get("path"))
		if err != nil {
			writeError(writer, err)
			return
		}

		json, err := json.Marshal(returnItem)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte(err.Error()))
			return
		}
		writer.WriteHeader(200)
		writer.Write(json)
		return
	case string(List):
		depth, err := strconv.Atoi(query.Get("depth"))
		if err != nil {
			depth = 1
		}

		returnItem, err := s.backend.ListDir(query.Get("path"), depth)
		if err != nil {
			writeError(writer, err)
			return
		}

		json, err := json.Marshal(returnItem)
		if err != nil {
			writeError(writer, err)
			return
		}

		writer.WriteHeader(200)
		writer.Write(json)
	default:
		writer.WriteHeader(404)
		writer.Write([]byte(fmt.Sprintf("Could not route command: %s", action)))
		return
	}
}

func (s *Server) handleFileUpload(body *multipart.Reader, path string) error {
	item := &backend.HfsItem{}
	var p string

	for {
		part, err := body.NextPart()
		if err == io.EOF {
			break
		}

		if part.FormName() == "file" {
			buf := new(bytes.Buffer)
			buf.ReadFrom(part)
			item.Content = buf.String()
		} else if part.FormName() == "filename" {
			buf := new(bytes.Buffer)
			buf.ReadFrom(part)
			item.Name = buf.String()
		} else if part.FormName() == "path" {
			buf := new(bytes.Buffer)
			buf.ReadFrom(part)
			p = buf.String()
		}
	}

	item.ModTime = time.Now()
	return s.backend.CreateFile(*item, filepath.Join(s.config.Root, p))
}

func writeError(writer http.ResponseWriter, err error) {
	writer.WriteHeader(500)
	writer.Write([]byte(err.Error()))
}
