package main

import (
	"fmt"
	"net/http"

	"gitlab.com/NateSchreiner/go-httpfs/config"
	"gitlab.com/NateSchreiner/go-httpfs/httpfs"
)

func main() {

	config := config.LoadConfig("default.json")
	serv := httpfs.NewServer(config)
	fmt.Printf("HTTP-FS listening on Port: %s \n", config.Port)

	err := http.ListenAndServe(fmt.Sprintf(":%s", config.Port), serv)
	if err != nil {
		panic(err)
	}

	// fsys := vfs.OS("/Users/nateschreiner/httpfs/")
	// fileInfo, err := fsys.ReadDir("/")
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(fileInfo)

}
